Viewing Source
===
PICO-8 cart scripts are stored in plaintext in .p8 files under the __lua tag. The --> line designates a different "module".

Simply open the p8 file of your choice in a text editor to view/edit source. 

List of Carts
===
lrd_dun.p8 - "LeftRightDown Dungeon" - a simple dungeon crawler with 3 inputs (left, right, down).
